package com.vrdtmr.entity;

import lombok.*;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "person_adress")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode(of = {"id"})
@ToString
public class Adress implements Serializable {
    private static final long serialVersionUID = 4754063645372841182L;


    @Id
    @SequenceGenerator(name = "seq_person_adres", allocationSize = 1)
    @GeneratedValue(generator = "seq_person_adres", strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(length = 500, name = "adress")
    private String adress;

    @Enumerated
    private AdresTip adresTip;

    @Column(name = "active")
    private Boolean active;

    @ManyToOne
    @JoinColumn(name = "person_adress_id")
    private Person person;

    public enum AdresTip {
        HOME_ADRESI,
        WORK_ADRESI,
        OTHER
    }
}
