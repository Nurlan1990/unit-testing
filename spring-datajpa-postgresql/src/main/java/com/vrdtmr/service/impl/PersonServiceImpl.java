package com.vrdtmr.service.impl;

import com.vrdtmr.dto.PersonDto;
import com.vrdtmr.entity.Adress;
import com.vrdtmr.entity.Person;
import com.vrdtmr.repo.AdressRepository;
import com.vrdtmr.repo.PersonRepository;
import com.vrdtmr.service.PersonService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class PersonServiceImpl implements PersonService {

    private final PersonRepository personRepository;
    private final AdressRepository adressRepository;

    @Override
    @Transactional
    public PersonDto save(PersonDto personDto) {
        Assert.notNull(personDto.getName(), "name is required!");

        Person person = new Person();
        person.setName(personDto.getName());
        person.setSurname(personDto.getSurname());
        final Person personDb = personRepository.save(person);

        List<Adress> liste = new ArrayList<>();
        personDto.getAdresses().forEach(item -> {
            Adress adres = new Adress();
            adres.setAdress(item);
            adres.setAdresTip(Adress.AdresTip.OTHER);
            adres.setActive(true);
            adres.setPerson(personDb);
            liste.add(adres);
        });
        adressRepository.saveAll(liste);
        personDto.setId(personDb.getId());
        return personDto;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public List<PersonDto> getAll() {
        List<Person> kisiler = personRepository.findAll();
        List<PersonDto> kisiDtos = new ArrayList<>();

        kisiler.forEach(it -> {
            PersonDto kisiDto =new PersonDto();
            kisiDto.setId(it.getId());
            kisiDto.setName(it.getName());
            kisiDto.setSurname(it.getSurname());
            kisiDto.setAdresses(
                    it.getAdresses() != null ?
                            it.getAdresses().stream().map(Adress::getAdress).collect(Collectors.toList())
                            : null);
            kisiDtos.add(kisiDto);
        });
        return kisiDtos;
    }

    @Override
    public Page<PersonDto> getAll(Pageable pageable) {
        return null;
    }
}
