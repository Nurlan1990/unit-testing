package com.vrdtmr.service.impl;

import com.vrdtmr.dto.PersonDto;
import com.vrdtmr.entity.Adress;
import com.vrdtmr.entity.Person;
import com.vrdtmr.repo.AdressRepository;
import com.vrdtmr.repo.PersonRepository;
import com.vrdtmr.service.PersonService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@ExtendWith(MockitoExtension.class)
class PersonServiceImplTest {

    @InjectMocks
    private PersonServiceImpl personServiceImpl;

    @Mock
    private PersonRepository personRepository;

    @Mock
    private AdressRepository adressRepository;



    @Test
    void testSave() throws Exception{
        PersonDto personDto = new PersonDto();
        personDto.setName("Marshall");
        personDto.setSurname("Bruce");
        personDto.setAdresses(Arrays.asList("adress1"));

        Person personMock = mock(Person.class);
        when(personMock.getId()).thenReturn(1L);

        when(personRepository.save(ArgumentMatchers.any(Person.class))).thenReturn(personMock);
        PersonDto result = personServiceImpl.save(personDto);

        assertEquals(result.getName(),personDto.getName());
        assertEquals(result.getId(),1L);

    }


    @Test
     void testSaveException() {
        PersonDto personDto = new PersonDto();
        personDto.setSurname("Test-Lastname");
        personDto.setAdresses(Arrays.asList("Test-Adress-1"));

        Assertions.assertThrows(IllegalArgumentException.class, () -> personServiceImpl.save(personDto));
    }

     @Test
     void testGetAll() {
        Person person = new Person();
        person.setId(1L);
        person.setName("Test-Name");
        person.setSurname("Test-Lastname");

        when(personRepository.findAll()).thenReturn(Collections.singletonList(person));
        List<PersonDto> kisiDtos = personServiceImpl.getAll();

        assertEquals(kisiDtos.size(), 1);
        assertEquals(kisiDtos.get(0), PersonDto.builder().id(1L).build());
    }

    @Test
    public void testGetAllWithAddress() {
        Person person = new Person();
        person.setId(1L);
        person.setName("Test-Name");
        person.setSurname("Test-Lastname");

        Adress adress = new Adress();
        adress.setAdresTip(Adress.AdresTip.OTHER);
        adress.setAdress("Test Adres");
        person.setAdresses(Collections.singletonList(adress));

        when(personRepository.findAll()).thenReturn(Collections.singletonList(person));
        List<PersonDto> kisiDtos = personServiceImpl.getAll();

        assertEquals(kisiDtos.get(0).getAdresses().size(), 1);
    }



}